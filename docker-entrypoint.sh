#!/bin/bash

DBHOST=${DBHOST-db}
CONF_HOME=${CONF_HOME-/data/teamcity_server/datadir}

case "$1" in
    package)
  [ -z "$2" ] && echo "Missing output file argument" && exit 1;
  [ -e "$2" ] && [ ! -w "$2" ] && echo "Target file is not writeable" && exit 1;

  # TMP File
  SQLFILENAME="pg_dumpall.sql";
  TMPFILE="$CONF_HOME/${SQLFILENAME}";

  # store sql
  pg_dumpall --clean -U teamcity -h $DBHOST > $TMPFILE;

  cd ${CONF_HOME}

  # compress
  tar czf "$2" *

  # Remove tmp file
  rm $TMPFILE;
  ;;
    restore)
  [ -z "$2" ] && echo "Missing output file argument" && exit 1;
  
  tar -x -C ${CONF_HOME} -f "$2"

  for i in $(seq 1 5);
  do
    psql -U teamcity -h "$DBHOST" -f ${CONF_HOME}/pg_dumpall.sql teamcity
    if [ "$?" == 0 ]; then
      
      break;
    else
      echo "PostgreSQL not accepting connection, retrying"
      sleep 2
    fi
  done

  ;;
    *)
  exec "$@"
  ;;
esac